import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Login } from '../models/LoginModel';
import { LoginService } from '../services/LoginService';

@Component({
    selector: 'login',
    templateUrl: '../views/LoginView.html'
})

export class LoginController{

    user: Login;

    constructor(private router: Router, private ls: LoginService){
        this.user = new Login('','', '', '')
    }

    login(){
        this.ls.login(this.user)
        .then(data => {
            console.log(data)
            this.router.navigate(['/messages'])
        })
        .catch(error => console.log(error))        
    }

    facebookLogin(){
        this.ls.facebookLogin()
        .then(data => {
            console.log('Success: ' + JSON.stringify(data))
            this.router.navigate(['/messages'])
        })
        .catch(error => {
            //Se già ho fatto l'acceso va in errore
            if(error && error=='Cannot read property \'setAccessToken\' of undefined'){
                this.router.navigate(['/messages'])
            }
            console.log(error)
        })
    }
    
}
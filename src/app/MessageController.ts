import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router'

//Model
import { Message } from '../models/MessageModel';

//Service
import { MessageService } from '../services/MessageService';

@Component({
    selector: 'message',
    templateUrl: '../views/MessageView.html'
})

export class MessageController{

    message: Message

    constructor(private ms: MessageService, private ar: ActivatedRoute){
        
        this.message = new Message()

        this.ar.params.subscribe(params => {
            console.log(params)
            if(params.id && params.id!=0){
                this.ms.getMessages(params.id)
                .then(data => {
                    this.message = new Message(data)
                })
                .catch(error => console.log(error))
            }
        })
        
        

    }
    
}
import { Component } from '@angular/core';
//Model
import { Message } from '../models/MessageModel';

//Service
import { MessageService } from '../services/MessageService';

@Component({
    selector: 'messages',
    templateUrl: '../views/MessagesView.html'
})

export class MessagesController{

    messages: Message[]

    constructor(private ms: MessageService){
        this.messages = [];
        this.ms.getMessages()
            .then(data => {
                if(data){
                    data.forEach(item => {
                        this.messages.push(new Message(item))
                    });
                }
            })
            .catch(error => console.log(error))

    }
}
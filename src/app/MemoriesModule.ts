import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FacebookModule } from 'ngx-facebook';
import { FormsModule } from '@angular/forms';

//Components
import { MemoriesController } from './MemoriesController';
import { LoginController } from './LoginController';
import { MessagesController } from './MessagesController';
import { MessageController } from './MessageController';

//Services
import { MessageService } from '../services/MessageService';
import { LoginService } from '../services/LoginService';

//Routing
import { RouterModule, Routes } from '@angular/router';

const appRoutes : Routes = [
    {path: 'login', component : LoginController},
    {path: 'messages', component : MessagesController},
    {path: 'message/:id', component : MessageController},
    {path: '',   redirectTo: '/login', pathMatch: 'full' }
]

@NgModule({
    //Array dei components
    declarations: [
                MemoriesController,
                MessagesController,
                MessageController,
                LoginController
            ],
    //Array delle dipendenze
    imports: [
        BrowserModule, 
        FormsModule,
        RouterModule.forRoot(appRoutes),
        HttpModule,
        FacebookModule.forRoot()
    ],
    //Array di services e altro
    providers: [MessageService, LoginService],
    //Components da istanziare all'avvio
    bootstrap: [MemoriesController]
})

export class MemoriesModule{

}
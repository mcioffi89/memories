export class Login{
    private username: string
    private password: string
    private accessToken: string
    private userId: string

    constructor(username: string, password: string, accessToken: string, userId: string){
        this.username = username
        this.password = password
        this.accessToken = accessToken
        this.userId = userId
    }

    getUsername(): string{
        return this.username
    }

    getPassword(): string{
        return this.password
    }

    getAccessToken(): string{
        return this.accessToken
    }

    setAccessToken(token: string){
        this.accessToken = token
    }

    setUserId(userId: string){
        this.userId = userId
    }
}
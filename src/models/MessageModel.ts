export class Message{
    private id: string
    private userId: string
    private title: string
    private content: string
    private sendDate: Date

    constructor(obj?){
        if(obj){
            Object.assign(this, obj);
        }else{
            this.id = '';
            this.userId = '';
            this.title = '';
            this.content = '';
            this.sendDate = null;
        }
    }

    getId() : string { return this.id }
    getUserId() : string { return this.userId }
    getTitle() : string { return this.title }
    getContent() : string { return this.content }
    getSendDate() : Date { return this.sendDate }

    
}
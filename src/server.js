var express = require('express');
var app = express();
const path = require('path');
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies


app.get('/message', function(req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true);
    if(req.query.id){
        var message = {id: 'id0', userId: 'user1', title: 'Titolo1', content: 'a me l Orlando Furioso è sempre sembrato tutto fumo e niente Ariosto', sendDate: new Date()};
                res.jsonp(message)
    }else{
        var messages = [];
        messages.push({id: 'id0', userId: 'user1', title: 'Titolo1', content: 'a me l Orlando Furioso è sempre sembrato tutto fumo e niente Ariosto', sendDate: new Date()})
        messages.push({id: 'id1', userId: 'user1', title: 'Titolo2', content: 'Con le mani sporche fai le macchie nere Vola sulle scope come fan le streghe Devi fare ciò che ti fa stare', sendDate: new Date()})
        messages.push({id: 'id3', userId: 'user1', title: 'Titolo3', content: 'Agata è bella, ha dei cavi in tasca vuole fare una strage al panificio', sendDate: new Date()})
        messages.push({id: 'id4', userId: 'user1', title: 'Titolo4', content: 'Tutti noi ce la prendiamo con la storia ma io dico che la colpa è nostra è evidente che la gente è poco seria quando parla di sinistra o destra. ', sendDate: new Date()})
        res.jsonp(messages)
    }    
});

app.post('/login', function(req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true);
    console.log(req.body.user)

    let message = {result: 'Succes'}
    res.jsonp(message)
});

app.listen(3000, function(){
    console.log('App sta ascolatando su localhost:3000');
});
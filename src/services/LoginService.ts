import { Injectable } from '@angular/core';
import { Http, Response , Headers, RequestOptions } from '@angular/http';
import { Login } from '../models/LoginModel';
import { FacebookService, InitParams , LoginResponse} from 'ngx-facebook';

//Per la gestione delle promise
import 'rxjs/add/operator/toPromise';

@Injectable()
export class LoginService{
    
    private endpoint = 'http://localhost:3000/'
    private user: Login


    constructor(private http: Http, private fb: FacebookService){
        let initParams: InitParams = {
            appId: '1927971220769787',
            version: 'v2.9'
          };
       
          this.fb.init(initParams);

    }

    login(user: Login): Promise<any>{

        let ep_login = this.endpoint + 'login'
        let headers = new Headers({'Content-type':'application/json', 'Accept': 'q=0.8;application/json;q=0.9'})
        let options = new RequestOptions({headers: headers})
        this.user = user

        debugger
        return this.http.post(ep_login, this.user, options)
            .toPromise()
            .then(response => {
                return response.json()
            })
            .catch(this.handleError)
    }

    facebookLogin(): Promise<any>{
        return this.fb.login()
        .then((response: LoginResponse) => {
            this.user.setAccessToken(response.authResponse.accessToken)
            this.user.setUserId(response.authResponse.userID)
            return response
        })
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any>{
        return Promise.reject(error.message || error)
    }

}
import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';

//Per la gestione delle promise
import 'rxjs/add/operator/toPromise';

@Injectable()
export class MessageService{
    
    private endpoint = 'http://localhost:3000/'

    constructor(private http: Http){
    }

    getMessages(id?: string): Promise<any>{

        let params = new URLSearchParams()
        let ep_message = this.endpoint + 'message'
        
        if(id){
            params.append('id', id)
        }
        
        return this.http.get(ep_message, {search: params})
            .toPromise()
            .then(response => {
                return response.json()
            })
            .catch(this.handleError)
    }

    private handleError(error: any): Promise<any>{
        return Promise.reject(error.message || error)
    }

}